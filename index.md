---
layout: default
title: Chaitanya
---
Hello there! I am Chaitanya.

I can’t say I love to read, listen and watch but I do read books and articles, listen to podcasts and watch a very small subset of videos and movies. I am enrolled in the Bachelor of Design programme at the Department of Design, IIT Guwahati. My interests in the field of design include typography, type design, and creative coding.

Mastodon: [@chaitanya@fosstodon.org](https://fosstodon.org/@chaitanya)\
Buy Me a Coffee: [buymeacoffee.com/chaitanya](https://www.buymeacoffee.com/chaitanya)

Have a good day!
			
