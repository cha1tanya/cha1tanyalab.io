---
layout: default
title:  "Hassle-free car scrapping"
date:   2020-10-22 15:15:15 +0530
categories: blog
---
Has your car reached its age? And it can’t even be sold to anyone because it is no more usable? Are you in Delhi? Then you are in luck! Just drive your car daily to a nearby Metro station. Do not park it in the designated parking spot. Also, do not park it in a no-parking zone. That would just create more problems. Instead, only find a location in a back alley where parking is permitted. Go to work through the metro. Come back in the evening, and you will find your car right there where you parked it. Repeat this exercise for about three weeks if your vehicle has not been stolen yet. If after three weeks it is still not stolen then just leave the door open and leave. Ta-da hassle-free car scrapping!