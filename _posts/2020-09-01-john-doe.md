---
layout: default
title:  "My 'John Doe' of songs"
date:   2020-09-01 15:15:15 +0530
categories: blog
---
Thrice in the last two years, I have spent hours on finding a single song. I didn’t remember the lyrics, the music associated with it and not even the singer’s name. What I remembered were the feelings or emotions that I have attached with the song. My “want” for the same feeling has led me to spend over 12 hours in three successful attempts at finding the same piece.

The most recent adventure full of frustration took place on 27 July 2020 when it took me 6 hours overnight to finally listen to [Tere Bin by Rabbi Shergill](https://www.youtube.com/watch?v=1IMzEh0dsxs).

The first search was because I hadn’t heard the song in years. After finding it, I saved it in a playlist on YouTube. The second one was when I deleted all my YouTube data and didn’t export or save my data beforehand. I kept it again on YouTube because Spotify didn’t have it in their library. The third search happened soon after also deleting both YouTube and Spotify and moving on to Apple Music. Sadly, they do not have it in their library too. This time I saved the YouTube link in my browser’s bookmarks because at least that’s one thing I make sure to export before changing browsers.

The third time was really tiring. At 1:30 am out of nowhere, my want for the song made me start searching for it. I kept searching for it till 5:45 am without any success. I went out for a walk and slept. When I woke up, I somehow finally had one super tiny part of the music in my brain. I again kept searching.

After 6 hours and going through the Wikipedia pages for [all Bollywood movies between 2000 and 2006](https://en.wikipedia.org/wiki/Lists_of_Bollywood_films#2000s), I finally found the song again! If I had gone through the list for 2007 early on, I would have found the page for the movie Delhii Heights in which the song was used.

I still can’t believe I spent so much time looking for a song. Only if I showed such determination or want for doing something in life. Though at the end I found my piece and I got to read about over one thousand Bollywood movies. That was like going back in the memory lane. Some of those movies were the first ones I ever watched. Some of the actors have died too. Even with all the frustration, it felt good to have got to know more about them all. And of course, when I finally heard the song, I had a rare moment of celebrating the completion of a process. Usually, I love the process and hate the end of it.

I do love Rabbi Shergill’s music. I have been listening a lot of his songs on repeat in the past one month.