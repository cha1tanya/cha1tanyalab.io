---
layout: default
title:  "Hiding tonight"
date:   2020-08-28 15:15:15 +0530
categories: blog
---
I rarely continue a habit for more than three days. If it is sleeping on time and getting up early morning, I’ll do it for three nights, and on the fourth night something just gets stuck in my thoughts. I am unable to go to sleep. It’s not like I don’t like sleeping early. I love getting up early after a good night’s sleep. There is no one to ask for my attention at that time. No distractions.

The fourth night is all about options. Either I can save myself from my thoughts and sleep, or I can face them and stay up. There is also an option that looks like it is going to help me, but it doesn’t – staying up and still not taking any action. I am hiding tonight.

I stay up till I am exhausted, and then I sleep. When I wake up, I mostly don’t remember what I did throughout the whole night. I don’t think I do anything worth remembering.

Every time I hope that next time is going to be different, but inaction continues. I know I need to change myself before I get habitual of this. But maybe it already is a habit.

**I need to change.**

The post is titled after one of Alex Turner’s songs in his debut [EP Submarine](https://album.link/i/422325910). I’ll recommend you to give it a listen.