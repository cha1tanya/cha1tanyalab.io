---
layout: default
title:  "I do not want to choose my meals"
date:   2020-09-19 15:15:15 +0530
categories: blog
---
Staying at the college campus since last July, I became used to not having much choice about what my breakfast, lunch and dinner would be. I went into the mess and just had what was there on the menu that day. I could choose what I eat between the meals, but I used to mostly go for tea or coffee with pakoras.

But now that I am back home and my mother is in charge of the kitchen, I am forced to choose between options or just tell her what I want to eat. I am unable to determine or even come up with any specific dish I am craving for. I’ve lost the art of foodly desires.

Though I am happy about losing mostly all cravings related to food, I do not feel good about not able to give an answer to my mother about what I would like to eat. To go around this, I am now merely choosing the first of the two options she gives me (or the one that is easier to cook) and when I have to say what I want to eat I again say a dish that is very simple to make and takes the least amount of her time.

I honestly do not want to choose what I eat. I want to eat whatever is readily available or cooked at a given time. I do not want to go to specific restaurants to eat but eat at random nearby places instead. Maybe, I just want my meals to be simple. And also to be just food and nutrition instead of attaching emotions with them.