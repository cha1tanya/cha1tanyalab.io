---
layout: default
title: Podcast
permalink: /podcast/
---
Weekly short episodes for the highs and lows; small yet impactful ideas. Occasional long candid conversations with guests.

- [Apple Podcasts](https://podcasts.apple.com/podcast/id1511066766)
- [Spotify](https://open.spotify.com/show/0Ag9bNP2x2ova48QpnzSEy)
- [Pocket Casts](https://pca.st/rfr5tkab)
- [Overcast](https://overcast.fm/itunes1511066766)
- [Listen on my website](https://chaitanya.page/podcast/)
- [RSS Feed](https://chaitanya.page/feed/podcast)